import React from 'react';
import homefondo from './../../../assets/img/homefondo.jpg';
import './../../../assets/styles/nav-vertical.css';
import logolat from './../../../assets/img/logolat.png';
import $ from 'jquery';
window.jQuery = $;

class Inicio extends React.Component {
    componentDidMount() {
        $('#mostrar-nav').on('click',function(){
            $('.nav-lat').toggleClass('mostrar');
        });
        $('#cerrar-nav').on('click',function(){
            $('.nav-lat').toggleClass('mostrar');
        })
    }
    render(){
        return(
            <div>
                <div id="mostrar-nav" class="body"></div>
                    <nav class="nav-lat">
                    <div id="cerrar-nav"></div>
                    <div>
                        <img src={logolat} class="logo"></img>
                    </div>
                        <ul class="menu">
                            <li><a href="/#/home"><b>inicio</b></a></li>
                            <li><a href="/#/fichas"><b>Ficha Tecnica</b></a></li>
                            <li><a href="#"><b>Servicios</b></a></li>
                        </ul>
                    </nav>
                <div>
                    <img src={homefondo} class="img-fluid"></img>
                </div>
            </div>
        )
    }
}
export default Inicio;
