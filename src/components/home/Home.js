import React from 'react';
import Menu from '../menu/Menu';
import Footer from '../footer/Footer';
import Inicio from './inicio/Inicio';

class Home extends React.Component {
    render(){
        return(
            <>
            <Menu/> 
			<main role="main" class="">
                <div class="">
				    <Inicio />
                </div>	
	  		</main>
            <Footer />
	  		</>
        )
    }
}
export default Home;





