import React from 'react';
import TtituloEditar from './tituloEditar/TtituloEditar';
import FormEditar from './formEditar/FormEditar';


class EditarFicha extends React.Component {
    render(){
        return(
            <>
            <TtituloEditar/> 
			<main role="main" class="">
                <div class="">
				    <FormEditar />
                </div>	
	  		</main>
	  		</>
        )
    }
}
export default EditarFicha;