import React from 'react';
import TituloLista from './tituloLista/TituloLista';
import TablaLista from './tablaLista/TablaLista';


class ListaFicha extends React.Component {
    render(){
        return(
            <>
            <TituloLista/> 
			<main role="main" class="">
                <div class="">
				    <TablaLista />
                </div>	
	  		</main>
	  		</>
        )
    }
}
export default ListaFicha;