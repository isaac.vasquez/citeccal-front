import React from 'react';
import TituloNueva from './tituloNueva/TituloNueva';
import FormNuevo from './formNuevo/FormNuevo';


class NuevaFicha extends React.Component {
    render(){
        return(
            <>
            <TituloNueva/> 
			<main role="main" class="">
                <div class="">
				    <FormNuevo />
                </div>	
	  		</main>
	  		</>
        )
    }
}
export default NuevaFicha;